﻿namespace SnakeGame
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gameCanvas1 = new SnakeGame.GameCanvas();
            this.SuspendLayout();
            // 
            // gameCanvas1
            // 
            this.gameCanvas1.BackColor = System.Drawing.Color.SteelBlue;
            this.gameCanvas1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gameCanvas1.Location = new System.Drawing.Point(0, 0);
            this.gameCanvas1.Name = "gameCanvas1";
            this.gameCanvas1.Size = new System.Drawing.Size(541, 490);
            this.gameCanvas1.TabIndex = 0;
            this.gameCanvas1.Text = "gameCanvas1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 490);
            this.Controls.Add(this.gameCanvas1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Змейка";
            this.ResumeLayout(false);

        }

        #endregion

        private GameCanvas gameCanvas1;
    }
}

