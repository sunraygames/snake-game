﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SnakeGame {
    /// <summary>
    /// Класс игрового поля
    /// </summary>
    public partial class GameCanvas : Control {
        #region Константы

        /// <summary>
        /// Ширина игрового поля, точек
        /// </summary>
        private const int AREA_WIDTH = 30;

        /// <summary>
        /// Высота игрового поля, точек
        /// </summary>
        private const int AREA_HEIGHT = 30;

        #endregion

        #region Данные

        /// <summary>
        /// Позиция точки
        /// </summary>
        private Point _dotPosition;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор класса
        /// </summary>
        public GameCanvas() {
            InitializeComponent();
            initCanvas();
        }

        #endregion

        #region Методы

        /// <summary>
        /// Наша инициализация игрового поля
        /// </summary>
        private void initCanvas() {
            //Определяем цвет фона
            BackColor = Color.SteelBlue;
            //Подписываемся на события
            KeyDown += onKeyDown;
            Resize += onResize;
            //Начальная позиция для точки
            _dotPosition = new Point(0, 0);
        }

        /// <summary>
        /// Перемещает точку
        /// </summary>
        /// <param name="dx">на сколько позиций переместить по горизонтали</param>
        /// <param name="dy">на сколько позиций переместить по вертикали</param>
        private void moveDot(int dx, int dy) {
            _dotPosition.X += dx;
            if (_dotPosition.X < 0) {
                _dotPosition.X = 0;
            }
            if (_dotPosition.X >= AREA_WIDTH) {
                _dotPosition.X = AREA_WIDTH - 1;
            }
            _dotPosition.Y += dy;
            if (_dotPosition.Y < 0) {
                _dotPosition.Y = 0;
            }
            if (_dotPosition.Y >= AREA_HEIGHT) {
                _dotPosition.Y = AREA_HEIGHT - 1;
            }
        }

        #endregion

        #region Обработчики событий

        /// <summary>
        /// Обработчик нажатия на кнопку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onKeyDown(object sender, KeyEventArgs e) {
            switch ((int) e.KeyCode) {
                case 65: //A, влево
                    moveDot(-1, 0);
                    break;
                case 68: //D, вправо
                    moveDot(1, 0);
                    break;
                case 83: //S, вниз
                    moveDot(0, 1);
                    break;
                case 87: //W, вверх
                    moveDot(0, -1);
                    break;
                default:
                    e.Handled = false;
                    return;
            }
            e.Handled = true;
            //перерисовка поля
            Refresh();
        }

        /// <summary>
        /// Обработчик события отрисовки поля
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            //вычисляем размеры точки
            float pointWidth = Width/(float) AREA_WIDTH;
            float pointHeight = Height/(float) AREA_HEIGHT;
            //рисуем точку
            e.Graphics.FillEllipse(new SolidBrush(Color.Snow), 
                _dotPosition.X*pointWidth,
                _dotPosition.Y*pointHeight,
                pointWidth,
                pointHeight);
        }

        /// <summary>
        /// Обработчик события изменения размера игрового поля
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void onResize(object sender, EventArgs e)
        {
            Refresh();
        }

        #endregion
    }
}